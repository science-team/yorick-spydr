func id_instrument_from_header(fh)
{
  extern user_read_image_fun;
  if (fits_get(fh,"INSTRUME")=="NICI") {
    user_read_image_fun = nici_read;
    return "NICI";
  }
  if (fits_get(fh,"INSTRUME")=="GSAOI") {
    user_read_image_fun = gsaoi_read;
    return "GSAOI";
  }
  user_read_image_fun = [];
  return;
}

func gsaoi_read(imname)
{
  im = array(0.0f,[2,2*2048+170,2*2048+170]);
  im(2049+170:,1:2048)    = fits_read(imname,hdu=2);
  im(1:2048,1:2048)       = fits_read(imname,hdu=3);
  im(1:2048,2049+170:)    = fits_read(imname,hdu=4);
  im(2049+170:,2049+170:) = fits_read(imname,hdu=5);
  return im;
}

func nici_read(imname)
{
  extern nici_array;
  im = im-im(,::-1);
  grow,nici_array,(fits_get(fh,"CBFW")?"watson":"holmes");
  return im;
}
